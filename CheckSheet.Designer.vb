﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CheckSheet
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdCheck = New System.Windows.Forms.DataGridView
        CType(Me.grdCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdCheck
        '
        Me.grdCheck.AllowUserToAddRows = False
        Me.grdCheck.AllowUserToDeleteRows = False
        Me.grdCheck.AllowUserToResizeColumns = False
        Me.grdCheck.AllowUserToResizeRows = False
        Me.grdCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCheck.Location = New System.Drawing.Point(13, 9)
        Me.grdCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.grdCheck.Name = "grdCheck"
        Me.grdCheck.ReadOnly = True
        Me.grdCheck.RowTemplate.Height = 21
        Me.grdCheck.Size = New System.Drawing.Size(373, 612)
        Me.grdCheck.TabIndex = 2
        '
        'CheckSheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(404, 634)
        Me.Controls.Add(Me.grdCheck)
        Me.Name = "CheckSheet"
        Me.Text = "CheckSheet"
        CType(Me.grdCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdCheck As System.Windows.Forms.DataGridView
End Class
