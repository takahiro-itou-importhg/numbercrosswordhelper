﻿Public Class CheckSheet

    Private m_frmMainForm As MainForm
    Private m_nMaxIndex As Integer
    Private m_numShowCols As Integer = 10
    Private m_numShowRows As Integer
    Private m_curSelNumber As Integer = PuzzleField.SquareType.FIELD_SQRTYPE_NOSELECT

    Public Sub changeSelectNumber(ByVal nNewIdx As Integer)
        Dim xSelCol As Integer = (nNewIdx - 1) Mod m_numShowCols
        Dim ySelRow As Integer = (nNewIdx - 1) \ m_numShowCols

        m_curSelNumber = nNewIdx

        With grdCheck
            .ClearSelection()

            For Y = 0 To .RowCount - 1
                With .Rows(Y)
                    For X = 0 To m_numShowCols - 1
                        With .Cells(X)
                            .Style.BackColor = Color.White
                        End With
                    Next
                End With
            Next
            With .Rows(ySelRow * 2)
                With .Cells(xSelCol)
                    .Style.BackColor = Color.Green
                End With
            End With
            With .Rows(ySelRow * 2 + 1)
                With .Cells(xSelCol)
                    .Selected = True
                End With
            End With
        End With
    End Sub

    Public Sub setupMainForm(ByRef lpMainForm As MainForm)
        m_frmMainForm = lpMainForm
    End Sub

    Public Sub updateCheckSheet()
        Dim objField As PuzzleField = m_frmMainForm.m_objField

        If objField Is Nothing Then Exit Sub

        Dim nMaxIdx As Integer = m_nMaxIndex

        With grdCheck
            Dim fntParent As Font = .Font
            Dim fntHint As Font = New Font(fntParent.Name, 14, fntParent.Style Or FontStyle.Bold)

            For Y = 0 To m_numShowRows - 1
                For X = 0 To m_numShowCols - 1
                    Dim nIdx As Integer = (Y * m_numShowCols) + X + 1
                    If (nIdx > nMaxIdx) Then Exit For

                    With .Rows(Y * 2).Cells(X)
                        .Style.Font = fntHint
                        .Value = nIdx
                    End With

                    With .Rows(Y * 2 + 1).Cells(X)
                        .Style.Font = fntHint
                        .Value = objField.m_tblChars(nIdx)
                    End With
                Next
            Next
        End With

    End Sub

    Private Sub CheckSheet_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If m_frmMainForm IsNot Nothing Then
            m_frmMainForm.notifyUnloadCheckSheet()
        End If
    End Sub

    Private Sub CheckSheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim nMaxIdx As Integer = 40

        If (m_frmMainForm Is Nothing) Then
            Exit Sub
        End If

        Dim objField As PuzzleField = m_frmMainForm.m_objField
        With Me
            .Left = m_frmMainForm.Right
            .Top = m_frmMainForm.Top
        End With

        If objField IsNot Nothing Then
            nMaxIdx = objField.m_nMaxIdx
        End If

        m_nMaxIndex = nMaxIdx
        m_numShowRows = (nMaxIdx + m_numShowCols - 1) \ m_numShowCols

        With grdCheck
            .RowCount = m_numShowRows * 2
            .ColumnCount = m_numShowCols

            For X = 0 To m_numShowCols - 1
                With .Columns(X)
                    .SortMode = DataGridViewColumnSortMode.NotSortable
                    .Width = 32
                End With
            Next

            For Y = 0 To m_numShowRows - 1
                .Rows(Y * 2).Height = 32
                .Rows(Y * 2 + 1).Height = 32
            Next
        End With

        updateCheckSheet()

    End Sub

    Private Sub grdCheck_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCheck.CellClick
        Dim xSelCol As Integer, ySelRow As Integer

        ' 選択したセルを取得する
        With grdCheck
            If (.CurrentCell Is Nothing) Then
                Exit Sub
            End If

            With .CurrentCell
                xSelCol = .ColumnIndex
                ySelRow = .RowIndex \ 2
            End With
        End With

        Dim nSelNumber As Integer = (ySelRow * m_numShowCols) + xSelCol + 1

        If (m_nMaxIndex < nSelNumber) Then Exit Sub
        changeSelectNumber(nSelNumber)

        If (m_frmMainForm Is Nothing) Then Exit Sub
        m_frmMainForm.changeSelectNumber(nSelNumber)

    End Sub

    Private Sub grdCheck_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCheck.CellDoubleClick
        Dim xSelCol As Integer, ySelRow As Integer

        ' 選択したセルを取得する
        With grdCheck
            If (.CurrentCell Is Nothing) Then
                Exit Sub
            End If

            With .CurrentCell
                xSelCol = .ColumnIndex
                ySelRow = .RowIndex \ 2
            End With
        End With

        Dim nSelNumber As Integer = (ySelRow * m_numShowCols) + xSelCol + 1

        If (m_nMaxIndex < nSelNumber) Then Exit Sub
        changeSelectNumber(nSelNumber)

        If (m_frmMainForm Is Nothing) Then Exit Sub
        m_frmMainForm.changeSelectNumber(nSelNumber)

        Dim objField As PuzzleField = m_frmMainForm.m_objField

        ' 文字を入力する
        m_frmMainForm.inputCharacter(objField, Me, nSelNumber)
    End Sub

End Class